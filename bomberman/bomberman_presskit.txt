﻿--------------------
Presskit Template
--------------------

--------------------
Images Required
--------------------
#240x420
#64x64
480x660 (or 960x1320) 
1024x768
950x150
#480x270 (or anyting in 16:9 ratio 1920x1080, 1600x900, 1280x720, 600x338, 480x270, 300x169)
#315x250
#588x331 (scale up from other 16:9 ratio)

--------------------
Required Videos
--------------------
Timelapse Creation
Trailer/Teaser
Gameplay
Let's Play

--------------------
Info & Text Required
--------------------

--------------------
Name: 

Bomberman
--------------------
Download Link : 

http://bit.ly/I3AJAF	
--------------------
Short Description / TagLine / Chapter:  (EN)

The good & old Bomberman
--------------------
Long Description: (EN)

As i alway liked playing bomberman it just seems obvious for me to program one myself. This Game features fast-paced two player action and different powerups. Note the mustache.

Created with C# and SFML.NET. Unfortunately i did not have the time to finish sounds for this one.
--------------------
Short Description / TagLine / Chapter:  (DE)

Das gute alte Bomberman
--------------------
Long Description: (DE)

Da ich Bomberman schon immer gerne gespielt habe, lag es nahe, auch einmal selber eine Version zu programmieren.
Bomberman bietet schnelle Action für zwischendurch für zwei Spieler. Es gilt, den Gegner zu sprengen, oder zumindest, sich nicht selber in die Luft zu jagen.
Desweiteren gibt es verschiedene Powerups. Achtet auf den Schnurrbart!

Bomberman wurde in C# und SFML.NET entwickelt. Leider hatte ich keine Zeit, Sounds zu implementieren.

--------------------
Release Date:

2013-11-01
--------------------
ReleaseMonth:

November 2013
--------------------
Additional Links:

None	
--------------------
Authors:

Simon Weis
--------------------
Authors Twitter:

@Laguna_999
--------------------
Platforms:

Windows, Linux

--------------------
Engine:

SFML
--------------------
Style:

Action
--------------------
Theme:

Bombs
--------------------
Type:

Multiplayer

--------------------
Projekttyp:

Spiel


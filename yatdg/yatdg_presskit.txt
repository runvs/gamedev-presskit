﻿--------------------
Presskit Template
--------------------

--------------------
Images Required
--------------------
240x420
64x64
480x660 (or 960x1320) 
480x270 (pewn)
1024x768
950x150
480x270 (or anyting in 16:9 ratio 1920x1080, 1600x900, 1280x720, 600x338, 480x270, 300x169)
315x250
588x331 (scale up from other 16:9 ratio)

--------------------
Required Videos
--------------------
Timelapse Creation

--------------------
Info & Text Required
--------------------

--------------------
Name: 

Yet Another Tower Defence Game

--------------------
Download Link : 

http://bit.ly/ZTT2k3
--------------------
Short Description / TagLine / Chapter:  (EN)

Cell & Career
--------------------
Long Description: (EN)

For the ZFX-Action 5 we thought about a Tower Defence game where you have to stop prison breaks.

You take over the role of a jailer, who is called to stop a prison break from the "cells". With waterguns, batons, and handcuffs you have to fight the prisoners back into their cells.
There are four different towers and three superpowers to beat the two levels. On top of that there is a full, epic soundtrack.

--------------------
Short Description / TagLine / Chapter:  (DE)

Zelle und Karriere

--------------------
Long Description: (DE)

Für den ZFX-Jam haben wir uns ein kleines Tower Defence Game ausgesucht.

Man schlüpft in die Rolle eines Gefängniswärters, der gerufen wird, um einen Ausbruch aus den "Zellen" zu verhindern. Mit Wasserwerfen, Schlagstöcken oder Handschellenwerfern muss man die Ausbrecher wieder in ihre Zelle verweisen.
Neben vier Towern und drei "Superkräften" gibt es zwei verschiedene Level zu spielen. Ausserdem gibt es einen Soundtrack und Soundeffekte.

--------------------
Release Date:

2014-10-23

--------------------
ReleaseMonth:

Okt 2014

--------------------
Additional Links:

https://www.youtube.com/watch?v=_-cNRyAkefU Timelapse
http://zfx.info/viewtopic.php?f=38&t=3595 ZFX-Thread
http://zfx.info/viewforum.php?f=38 TFX-Action

--------------------
Authors:

Laguna, Thunraz

--------------------
Authors Twitter:

@Laguna_999 @Thunraz

--------------------
Platforms:

Windows

--------------------
Engine:

SFML.NET

--------------------
Style:

TD

--------------------
Theme:

Prison Break


--------------------
Type:

Singleplayer

--------------------
Projekttyp:

Spiel


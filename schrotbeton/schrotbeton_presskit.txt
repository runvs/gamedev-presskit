﻿--------------------
Presskit Template
--------------------

--------------------
Images Required
--------------------
#240x420
#64x64
480x660 (or 960x1320) 
1024x768
950x150
#480x270 (or anyting in 16:9 ratio 1920x1080, 1600x900, 1280x720, 600x338, 480x270, 300x169)
315x250
588x331 (scale up from other 16:9 ratio)

--------------------
Required Videos
--------------------
Timelapse Creation
Trailer/Teaser
Gameplay
Let's Play

--------------------
Info & Text Required
--------------------

--------------------
Name: 

Schrotbeton

--------------------
Download Link : 

http://bit.ly/U7oR5S

--------------------
Short Description / TagLine / Chapter:  (EN)

You only get one die.

--------------------
Long Description: (EN)

You only got on die. This is the basic rule for SchrotBeton. Every action is decided by a D6 roll.

On your epic quest you hunt a goblin tribe and its master - the mighty headless goblin - throughout the country. We got a decent battlesystem implemented as well as a really nice system for items (up to 1111 items can be randomly generated) and a small plot with some quests.

This is the updated post-jam version with some more juice.

Controls: WASD or arrows for movement Space: attack LCtrl: block LShift: spell L: Quest log E: Pickup/interact(houses)

This was our Entry for the LD48 #28 in Dec 2013.

--------------------
Short Description / TagLine / Chapter:  (DE)

Nur ein Würfel!
--------------------
Long Description: (DE)

Nur ein Würfel. Das ist die grundlegende Regel für SchrotBeton. Jede Aktion wird von einem W6 entschieden.

In einer epischen Geschichte jagst du  einen Goblinstamm und seinen Anführer - den mächten kopflosen Goblin - durch das ganze Land. 
Wir haben ein respektables Kampfsystem implementiert, ebenso ein System zur Erzeugung von Items (bis zu 1111 individuelle items können komplett zufällig generiert werden) 
und einen spannenden Plot umgesetzt.

Der Link bezieht sich auf die überarbeitete Post-LD-Version des Spiels.

Steuerung: WASD oder Pfeiltasten zum Bewegen
Space: Angriff 
LCtrl: Blocken 
LShift: Zaubern 
L: Quest log 
E: Aufheben, interagieren (Häuser)

Schrotbeton war unser Beitrag zum LD72 #28 im Dezember 2013.

--------------------
Release Date:

2014-12-16

--------------------
ReleaseMonth:

Dec 2013
--------------------
Additional Links:

Ludum Dare Page http://www.ludumdare.com/compo/ludum-dare-28/?action=preview&uid=29172

Gameplay Video https://www.youtube.com/watch?v=KyReiYn5tGQ&feature=youtu.be

Timelapse Video https://www.youtube.com/watch?v=Hdqypfckhsw

--------------------
Authors:

Simon Weis
Julian Dinges
--------------------
Authors Twitter:

@Laguna_999 @Thunraz
--------------------
Platforms:

Windows, 

--------------------
Engine:

SFML.NET 2.1
--------------------
Style:

Fantasy RPG
--------------------
Theme:

Fantasy
--------------------
Type:

Singleplayer

--------------------
Projekttyp:

Spiel

